from time import sleep
import cv2
from camera import VideoCamera
import socket

def gen(camera):
    #s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    #port = 8000
    #s.bind(('',port))
    #s.listen(5)
    #c,addr = s.accept()
    #print('Got connection from', addr )
    while True:
        frame,pred = camera.get_frame()
        cv2.imshow("Facial Expression",frame)
        #c.send(pred.encode())

        if cv2.waitKey(1) & 0xFF == ord('q'):
            #c.send("quit".encode())
            #sleep(1)
            #c.close()
            break

    cv2.destroyAllWindows()

def video_feed():
    return gen(VideoCamera())


if __name__ == "__main__":
    video_feed()
