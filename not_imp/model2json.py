import json
import tensorflow as tf

loaded_model = tf.keras.models.load_model("CNNModel_feraligned+ck_5emo.h5")
model_json = loaded_model.to_json()
with open("model5.json", "w") as json_file:
    json_file.write(model_json)