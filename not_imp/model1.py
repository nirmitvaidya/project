import tensorflow as tf
import numpy as np

class FacialExpressionModel(object):
    EMOTIONS_LIST = ["Angry", "Disgust",
                     "Fear", "Happy",
                     "Neutral", "Sad",
                     "Surprise"]

    def __init__(self, model_weights_file):
        self.model_path = model_weights_file
        self.interpreter = tf.lite.Interpreter(self.model_path)
        self.interpreter.allocate_tensors()
    
    def predict_emotion(self, img):
        self.preds = FacialExpressionModel.predictor(self,img)
        #print(np.argmax(self.preds))
        return FacialExpressionModel.EMOTIONS_LIST[np.argmax(self.preds)]

    def predictor(self, image):
        
        tensor_index = self.interpreter.get_input_details()[0]['index']
        input_tensor = self.interpreter.tensor(tensor_index)()[0]
        input_tensor[:, :] = image

        self.interpreter = tf.lite.Interpreter(self.model_path)
        self.interpreter.allocate_tensors()
        self.interpreter.invoke()
        output_details = self.interpreter.get_output_details()[0]
        output = self.interpreter.get_tensor(output_details['index'])
        #print(output)
        
        # scale, zero_point = output_details['quantization']
        # output = scale * (output - zero_point)
        
        return output       
